# Icelandic L2 Error Corpus

Version 0.1

Copyright 2020 Anton Karl Ingason, Lilja Björk Stefánsdóttir, Þórunn Arnardóttir, Xindan Xu.

Contact: anton.karl.ingason@gmail.com

License: Creative Commons Attribution 4.0 International (CC BY 4.0; See repository for text)

The Icelandic L2 Error Corpus is a collection of texts in modern Icelandic written by learners of Icelandic as a second language. They have been annotated for mistakes related to spelling, grammar, and other issues.

The project is funded by the Icelandic Government as a part of the Language Technology Programme for Icelandic 2019–2023 which is described in the following publication: 

Anna Björk Nikulásdóttir, Jón Guðnason, Anton Karl Ingason, Hrafn Loftsson, Eiríkur Rögnvaldsson, Einar Freyr Sigurðsson, Steinþór Steingrímsson. 2020. Language Technology Programme for Icelandic 2019–2023. Proceedings of LREC 2020 (https://arxiv.org/pdf/2003.09244.pdf)

# Íslensk L2 villumálheild

Útgáfa 0.1

Copyright 2020 Anton Karl Ingason, Lilja Björk Stefánsdóttir, Þórunn Arnardóttir, Xindan Xu.

Tengiliður: anton.karl.ingason@gmail.com

Leyfi: Creative Commons Attribution 4.0 International (CC BY 4.0; sjá leyfistexta í gagnaskjóðu).

Íslenska L2 villumálheildin er safn texta á nútímaíslensku sem hafa verið skrifaðir af annarsmálshöfum íslensku. Textarnir hafa verið merktir fyrir villum, t.d. hvað varðar stafsetningu, málfræði og fleira.

Þetta verkefni er fjármagnað af ríkissjóði Íslands sem hluti af Máltækniáætlun fyrir íslensku 2019-2023. Máltækniáætluninni er nánar lýst í eftirfarandi grein:

Anna Björk Nikulásdóttir, Jón Guðnason, Anton Karl Ingason, Hrafn Loftsson, Eiríkur Rögnvaldsson, Einar Freyr Sigurðsson, Steinþór Steingrímsson. 2020. Language Technology Programme for Icelandic 2019–2023. Proceedings of LREC 2020 (https://arxiv.org/pdf/2003.09244.pdf)
